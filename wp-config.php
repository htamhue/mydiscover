<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mydiscover' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[ZvD@7=Yy|8fU:Ol,FrNlklSwB4$< x-lyjTHQ2cnOx|QsClo/8)bWu$=6RbGA)O' );
define( 'SECURE_AUTH_KEY',  'o[B]zHtKZ:.mdo#/PEo5ulI}nZlH<ay(~sIbLx5b#SWu:( y4#Q9v+}yEvQR<#`@' );
define( 'LOGGED_IN_KEY',    '9/iAO960|Wc#zt1z2qp.{tyu4KG_rY^nxMIV=!&:us%<>V$S.nZGQS0nSOV!)<>l' );
define( 'NONCE_KEY',        'i5u]<PUPMJtRhd@QEt8c+ !|jL{$C]uYebU866`5Pv ~8t*,k;%/Lw(Qh.<FkamQ' );
define( 'AUTH_SALT',        'Nc=zfJJ?NR8/X39<wz72{u?O8/}:V`%gtq3-*b{yN({-Cl1Wx?8Y5$OVFoQRsr9O' );
define( 'SECURE_AUTH_SALT', ';tNP(eqTCjnJ9RTUj8),nt[TGT{YA<-bcg$9dHK$LKrQs$HJ[xxg(3.9oq-[fANC' );
define( 'LOGGED_IN_SALT',   '_LQw<[l&^LE:,>hIT=QGUG[kT~H0Lwwx7m/kS*Fp:r%gM-N>S[wW7X=b(,R5ya1v' );
define( 'NONCE_SALT',       'Uopb/ix5DG~r1b2e? ^o8`}:O-vag~_k,KeJIo29Z@43sBga%^8:j#LqObad#lUj' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
